import ConverCaseUtil from "../utils/convertCase";

class ApiResponse {
  private _status: number | null;
  constructor(private _response: Response | null, private _json: Object = {}) {
    this._status = _response ? _response.status : null;
  }

  get response(): Response | null {
    return this._response;
  }
  get json(): Object {
    return this._json;
  }
  get status(): number | null {
    return this._status;
  }

  public async fetchJson() {
    if (!this._response) return;
    try {
      const json = await this._response.json();
      const camelizedJson = ConverCaseUtil.snakeToCamelObj(json);
      this._json = camelizedJson;
    } catch (e) {
      console.log(e);
    }
  }
  public ok(): boolean {
    if (!this._response) return false;
    return this._response.status >= 200 && this._response.status < 300;
  }
}

export default ApiResponse;
