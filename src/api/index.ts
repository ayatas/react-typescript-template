import ConvertCaseUtil from "../utils/convertCase";
import QueryUtil from "../utils/query";

import { Query } from "../types/models";

import { API_END_POINT } from "../constants/env";
import ApiResponse from "./apiResponse";

type Method = "GET" | "POST" | "PUT" | "DELETE";

interface RequestOptions {
  method: Method;
  headers: Headers;
  mode: string;
  body?: string | FormData;
}

class Api {
  public async request(
    path: string,
    method: Method,
    { query = {}, data = {} }: { query?: Query; data?: Object } = {}
  ): Promise<ApiResponse> {
    const decamelizedQuery = ConvertCaseUtil.camelToSnakeObj(query as Object);
    const decamelizedData = ConvertCaseUtil.camelToSnakeObj(data as Object);

    const url = API_END_POINT + path + QueryUtil.stringify(decamelizedQuery);

    const options: RequestOptions = {
      method,
      headers: new Headers(),
      mode: "cors"
    };
    if (["POST", "PUT"].indexOf(method) >= 0) {
      options.body = JSON.stringify(decamelizedData);
    }

    let response: Response | null = null;

    try {
      response = await fetch(url, options as RequestInit);
      this.handleHttpError(response);
    } catch (error) {
      console.log(error);
    }

    const apiResponse = new ApiResponse(response);
    await apiResponse.fetchJson();
    return apiResponse;
  }

  private handleHttpError(response: Response) {
    if (response.status === 401) {
      console.log(401);
    }
  }
}

export default new Api();
