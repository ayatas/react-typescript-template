import * as React from "react";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";

import UI from "../../../theme/ui";

const Wrapper = styled.div``;

interface Props {}
interface State {}

export default class Main extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return <Wrapper>Hello World!</Wrapper>;
  }
}
