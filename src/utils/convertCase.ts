class ConverCaseUtil {
  static snakeToCamelObj(obj: { [key: string]: any } | Array<any>): Object {
    const {
      isObject,
      isArray,
      snakeToCamelStr,
      snakeToCamelObj,
      copy
    } = ConverCaseUtil;

    let newObj: Object | Array<any> = copy(obj);

    if (isArray(obj)) {
      newObj = (obj as Array<any>).map(item => snakeToCamelObj(item));
    } else if (isObject(obj)) {
      newObj = {};
      for (const key in obj) {
        if (!obj.hasOwnProperty(key)) continue;
        const item = (obj as { [key: string]: any })[key];
        (newObj as { [key: string]: any })[
          snakeToCamelStr(key)
        ] = snakeToCamelObj(item);
      }
    } else {
      newObj = obj;
    }
    return newObj;
  }

  static camelToSnakeObj(obj: { [key: string]: any } | Array<any>): Object {
    const {
      isObject,
      isArray,
      camelToSnakeObj,
      camelToSnakeStr,
      copy
    } = ConverCaseUtil;

    let newObj: Object | Array<any> = copy(obj);

    if (isArray(obj)) {
      newObj = (obj as Array<any>).map(item => camelToSnakeObj(item));
    } else if (isObject(obj)) {
      newObj = {};
      for (const key in obj) {
        if (!obj.hasOwnProperty(key)) continue;
        const item = (obj as { [key: string]: any })[key];
        (newObj as { [key: string]: any })[
          camelToSnakeStr(key)
        ] = camelToSnakeObj(item);
      }
    } else {
      newObj = obj;
    }
    return newObj;
  }

  static snakeToCamelStr(str: string) {
    return str.replace(/_./g, s => s.charAt(1).toUpperCase());
  }
  static camelToSnakeStr(str: string) {
    return str.replace(/([A-Z])/g, s => `_${s.charAt(0).toLowerCase()}`);
  }

  static isObject(obj: Object) {
    return obj instanceof Object && !(obj instanceof Array) ? true : false;
  }
  static isArray(obj: Object) {
    return obj instanceof Array ? true : false;
  }

  static copy(obj: Object) {
    return JSON.parse(JSON.stringify(obj));
  }
}

export default ConverCaseUtil;
