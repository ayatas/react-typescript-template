import qs from "qs";

import { Query } from "../types/models";
import ConvertCaseUtil from "./convertCase";

export default class QueryUtil {
  static stringify(params: { [key: string]: any }): string {
    const { isValidQuery } = QueryUtil;

    let str = "";

    const validKeys = Object.keys(params).filter(key =>
      isValidQuery(params[key])
    );
    if (validKeys.length > 0) {
      str += "?";
    }
    validKeys.forEach((key, index, self) => {
      let query = `${key}=`;
      if (ConvertCaseUtil.isArray(params[key])) {
        query += params[key].join(",");
      } else {
        query += params[key];
      }
      str += query;
      if (index < self.length - 1) {
        str += "&";
      }
    });

    return str;
  }

  static parse(str: string): Query {
    const { isValidQuery } = QueryUtil;

    const queries = qs.parse(str.replace(/\?/g, ""));
    const keys = Object.keys(queries);
    const params: Query = {};
    keys.forEach(key => {
      if (isValidQuery(queries[key])) {
        params[key] = queries[key].includes(",")
          ? queries[key].split(",")
          : queries[key];
      }
    });
    return ConvertCaseUtil.snakeToCamelObj(params);
  }

  static isValidQuery(value: any) {
    return !(value === undefined || value === null || value === "");
  }
}
