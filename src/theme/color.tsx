const Color = {
  white: "#fff",
  gray: "#c9c9c9",
  overlay25: "rgba(0, 0, 0, 0.25)",
  overlay50: "rgba(0, 0, 0, 0.50)",
  overlay75: "rgba(0, 0, 0, 0.75)"
};

export default Color;
