import { injectGlobal } from "styled-components";

injectGlobal`
  body {
  font-family: -apple-system, BlinkMacSystemFont, "Helvetica Neue", YuGothic,
    "ヒラギノ角ゴ ProN W3", Hiragino Kaku Gothic ProN, Arial, "メイリオ", Meiryo,
    sans-serif;
  }
  a {
    text-decoration: none;
  }
`;
