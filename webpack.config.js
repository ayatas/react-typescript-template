require("dotenv").config();
const webpack = require("webpack");
const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

const src = path.resolve(__dirname, "src");
const dist = path.resolve(__dirname, "dist");

module.exports = {
  entry: src + "/index.tsx",
  output: {
    path: dist,
    filename: "bundle.js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "ts-loader"
        }
      },
      { test: /\.css$/, use: ["style-loader", "css-loader"] },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: "url-loader"
          }
        ]
      },
      {
        test: /\.svg$/,
        use: {
          loader: "svg-url-loader",
          options: { noquotes: true }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: src + "/index.html"
    }),
    new webpack.DefinePlugin({
      API_END_POINT: JSON.stringify(process.env.API_END_POINT)
    })
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  devtool: "source-map",
  devServer: {
    historyApiFallback: true,
    contentBase: dist,
    port: 8080,
    host: "0.0.0.0",
    inline: true
  }
};
